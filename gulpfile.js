var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    gulp.src('./style/*.scss') 
    .pipe(sass().on('error', sass.logError))  
    .pipe(gulp.dest('./css')) 
});

gulp.task('watchSass', function() {
    gulp.watch('./style/*', ['sass']);
});

gulp.task('default', ['watchSass', 'sass']);