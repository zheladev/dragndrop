shapes = [
    'CircleShape',
    'SquareShape',
    'RectangleShape',
    'StarShape'
];

figures = [
    'Circle',
    'Square',
    'Rectangle',
    'Star'
];

window.onload = () => {
    let shapeContainer = document.getElementById('shapes');
    let figureContainer = document.getElementById('figures');

    addImages(shapes, shapeContainer, true);
    addImages(figures, figureContainer);
}

addImages = (imageArr, node, isShape = false) => {
    let _div;
    let _createImgNode = (_img) => {
        let _imgNode = document.createElement('img');
        _imgNode.src = `images\\${_img}.png`;
        _imgNode.id = _img;
        if (!isShape) {
            _imgNode.addEventListener('dragstart', drag, false)
            _imgNode.draggable = true;
        }
        
        //_imgNode.style.float = 'left';


        return _imgNode;
    }
    shuffle(imageArr);
    for (let i = 0; i < imageArr.length;i++) {
        _div = document.createElement('div');
        _div.id = imageArr[i] + '-div';
        _div.className = 'imgContainer';
        if (isShape) {
            _div.addEventListener('dragover', function(e) {e.preventDefault()}, false);
            _div.addEventListener('drop', drop, false);
        }

        _div.appendChild(_createImgNode(imageArr[i]));
        node.appendChild(_div);
    }
    
}


drop = (e) => {
    e.preventDefault();
    
    var data = e.dataTransfer.getData("figure");
    console.log(data);
    if (e.target.id.indexOf(data)>=0) {
        e.target.className = 'matchedShape';
        console.log(e + " " + e.className);
        document.getElementById(data).className = 'matched';
        console.log(document.getElementById(data) + " " + document.getElementById(data).className);
        e.target.parentNode.appendChild(document.getElementById(data));
    }
   
}


drag = (e) => {
    e.dataTransfer.setData("figure", e.target.id);
}

shuffle = (array) => {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
